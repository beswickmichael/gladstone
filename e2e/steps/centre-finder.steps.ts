import { AppPage } from '../page';
import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';
import { ElementFinder } from 'protractor';

let app: AppPage;

Given('that I want to see a list of centres', async () => {
    app = new AppPage();

    await app.join.navigateTo();
});

When('I do nothing', () => {
    return true;
});

Then('the centre finder location input should exist', () => {
    expect(app.join.centreFinder.locationInput.isDisplayed()).to.be.true;
});

Then('the location suggest list should exist', () => {
    expect(app.join.centreFinder.suggestionsList.isDisplayed()).to.be.false;
});


Given('I navigate to the join page', async () => {
    app = new AppPage();

    await app.join.navigateTo();
});

Then('expect the location suggestion list to exist', async () => {
    expect(app.join.centreFinder.suggestionsList.isDisplayed()).to.be.true;
});

Then('the location suggestion list first item to be "{string}"', async (string) => {
    expect(app.join.centreFinder.suggestionsList.all('*').first().getText()).to.be(string);
});

When('enter the {string} in the location field', async (string) => {
    await app.join.centreFinder.locationInput.sendKeys(string);
});

When('I select the first item in the autocomplete', async () => {
    await (await app.join.centreFinder.suggestionsList.all('*').first()).click();
});

Given('have selected my location', async () => {
    await app.join.navigateTo();
    await app.join.centreFinder.locationInput.sendKeys('SW152LL');
    await (app.join.centreFinder.suggestionsList.all('*').first()).click();
});

Then('the fist matching centre should have the title {string}', async (string) => {
    const title = await app.join.centreFinder.matchingCentres;

    await expect(title.getText()).to.be.equal(string);
});

Then('the centres list should exist', async () => {
    expect(app.join.centreFinder.matchingCentres.isDisplayed()).to.be.true;
});

Then('the centres list should contain {string} items', async (string) => {
    const firstCentreCard = await app.join.centreFinder.matchingCentres.all('*').length;

    expect(firstCentreCard.length).to.be(string);
});

Then('the centre finder should mutate into a panel with the text {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('and edit icon should exit', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('a centre card text {string}', async (string) => {
    const text = await app.join.centreFinder.matchingCentres.all('*').first().getText();

    expect(text).contain(string);
});

Then('the panel should be collapsed', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('I click the button {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('should contain an expand icon', async () => {
    return 'pending';
});

When('I click on expand icon in the tags panel', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the tags pane should expand', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the collapse icon should exist', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Given('expanded the filter panel', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('i click on collapse icon in the tags panel', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the tags pane should collapse', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Given('expanded the filter panel', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('tags with the text {string} and {string} should exist', function (string, string2) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('label with the text {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('a select with the default option {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the first centre card should contain an element with the text {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('a button with the text {string} should exist', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('a button with the text {string} should exist', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('I select {string} as the distance', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the Centre list should update', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('an element with the text {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('I click on the tag {string}', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('it should not be selected', () => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Given('selected the {string} tag', async (string) => {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});