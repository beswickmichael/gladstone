Feature: Centre finder results
    As a user
    I should be able to search for centres using criteria
    In order to choose a suitable centre
    
    Background:
        Given that I want to see a list of centres
        When enter "SW152LL" in the location field
        And I select the first item in the autocomplete

    Scenario: Max of 3 centres in list
        Given have selected my location
        Then the fist matching centre should have the title "Your location"
        And the centres list should exist
        And the centres list should contain "3" items

    Scenario: Filter panel displayed
        Given have selected my location
        Then a centre card text "are you looking for a particular activity"
        And the panel should be collapsed
        And should contain an expand icon

    Scenario: Filter panel expands
        Given have selected my location
        When i click on expand icon in the tags panel
        Then the tags pane should expand
        And the collapse icon should exist

    Scenario: Filter panel collapses
        Given have selected my location
        And expanded the filter panel
        When i click on collapse icon in the tags panel
        Then the tags pane should collapse

    Scenario: Filter panel display when expanded
        Given have selected my location
        And expanded the filter panel
        Then tags with the text "gym" and "swim" should exist
        And label with the text "distance"
        And a select with the default option "5 miles"

    Scenario: Filter when tag selected
        Given have selected my location
        And expanded the filter panel
        When I click on the tag "gym"
        Then it should be selected
        And the Centre list should update

    Scenario: Filter when tag unselect
        Given have selected my location
        And expanded the filter panel
        And selected the "gym" tag
        When I click on the tag "gym"
        Then it should not be selected
        And the Centre list should update

    Scenario: Filter when distance changed
        Given have selected my location
        And expanded the filter panel
        When I select "10 miles" as the distance
        Then the Centre list should update

    Scenario: Centre card
        Given have selected my location
        Then the first centre card should contain an element with the text "location name"
        And a button with the text "more info" should exist
        And a button with the text "choose centre" should exist
        And an element with the text "0.5 miles away"

    Scenario: Centre card select
        Given have selected my location
        When I click the button "choose this centre"
        Then the centre finder should mutate into a panel with the text "location name"
        And and edit icon should exit