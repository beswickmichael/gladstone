Feature: Centre finder location input
    As a user
    I should be able lookup a location by name or post code

    Scenario Outline: Scenario Outline name: Centres displayed for location
        Given I navigate to the join page
        When enter the "<string> in the location field
        Then expect the location suggestion list to exist
        And the location suggestion list first item to be "<found>"

        Examples:
            | location     | found                |
            | "Water Road" | "Water Road, London" |
            | "SW152LL"    | "Water Road, London" |
            | "QAZ45QW"    | "not found"          |