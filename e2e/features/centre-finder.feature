Feature: Centre finder
    As a user
    I should be able to search for centres using criteria
    In order to choose a suitable centre

    Scenario: Default state
        Given that I want to see a list of centres
        When I do nothing
        Then the centre finder location input should exist
        And the location suggest list should exist
