import { browser, by, element, ElementFinder, WebElement } from 'protractor';
import Join from './join';

export class AppPage {
  join: Join;

  constructor() {
    this.join = new Join();
  }

  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }
}
