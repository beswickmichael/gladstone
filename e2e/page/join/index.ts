import { browser, by, element, ElementFinder, WebElement } from 'protractor';
import CentreFinder from './centre-finder';

export default class AppPage {
    centreFinder: CentreFinder;

    constructor() {
        this.centreFinder = new CentreFinder();
    };

    navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl + '/join') as Promise<unknown>;
    }

}