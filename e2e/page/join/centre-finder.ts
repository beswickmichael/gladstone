import { browser, by, element, ElementFinder, WebElement, Locator, WebElementPromise } from 'protractor';

export default class CentreFinder {
  get locationInput(): ElementFinder {
    return element(by.binding('location'));
  };

  get suggestionsList(): ElementFinder {
    return element(by.repeater('locationSuggestions'));
  }

  get matchingCentres(): ElementFinder {
    return element(by.repeater('matchingCentres'));
  }
}
