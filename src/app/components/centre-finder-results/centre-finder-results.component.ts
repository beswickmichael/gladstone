import { Component, OnInit } from '@angular/core';
import {
  faDraftingCompass,
} from '@fortawesome/free-solid-svg-icons';

type CentreInfo = {
  name: string;
  distance: number;
  tags: string[];
};

const matches: CentreInfo[] = [
  {
    name: "Centre name",
    distance: 0.75,
    tags: ['gym', 'swim']
  },
  {
    name: "Centre name",
    distance: 0.75,
    tags: ['gym', 'swim']
  }, {
    name: "Centre name",
    distance: 0.75,
    tags: ['gym', 'swim']
  }
];

@Component({
  selector: 'app-centre-finder-results',
  templateUrl: './centre-finder-results.component.html',
  styleUrls: ['./centre-finder-results.component.scss']
})
export class CentreFinderResultsComponent implements OnInit {
  faDraftingCompass = faDraftingCompass;
  matches: CentreInfo[];

  constructor() { }

  ngOnInit(): void {
    this.matches = matches;
  }

}
