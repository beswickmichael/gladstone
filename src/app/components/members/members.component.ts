import { Component, OnInit } from '@angular/core';
import {
  faPen,
  faLocationArrow,
  faQuestionCircle,
  faUser,
  faCheck,
  faSearchLocation,
  faDraftingCompass
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-member-block',
  templateUrl: './member-block.component.html',
  styleUrls: ['./member-block.component.scss']
})
export class MemberBlockComponent implements OnInit {
  faPen = faPen;
  faLocationArrow = faLocationArrow;
  faQuestionCircle = faQuestionCircle;
  faUser = faUser;
  faCheck = faCheck;
  faSearchLocation = faSearchLocation;
  faDraftingCompass= faDraftingCompass;

  constructor() { }

  ngOnInit(): void {
  }

}
