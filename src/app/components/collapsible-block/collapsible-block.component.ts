import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {
  faAngleUp,
  faAngleDown,
} from '@fortawesome/free-solid-svg-icons';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-collapsible-block',
  templateUrl: './collapsible-block.component.html',
  styleUrls: ['./collapsible-block.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({ height: '*' })),
      state('false', style({ height: '0px' })),
      transition('false <=> true', animate('250ms ease-in-out'))
    ])
  ]
})
export class CollapsibleBlockComponent implements OnInit {
  @ViewChild('collapsible-content') content: ElementRef;
  @Input('isCollapsed') isCollapsed: boolean = false;
  @Input('title') title: string;

  get titleIcon() {
    return this.isCollapsed ? faAngleUp : faAngleDown;
  }

  constructor() { }

  ngOnInit(): void {

  }

  toggle() {
    this.isCollapsed = !this.isCollapsed;
  }


}
