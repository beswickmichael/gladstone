import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentreFinderLocationComponent } from './centre-finder-location.component';

describe('CentreFinderLocationComponent', () => {
  let component: CentreFinderLocationComponent;
  let fixture: ComponentFixture<CentreFinderLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentreFinderLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentreFinderLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
