import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {
  faLocationArrow,
  faSearchLocation,
  IconDefinition
} from '@fortawesome/free-solid-svg-icons';
import GoogleMapsApiLoader from 'google-maps-api-loader';

const apiKey = 'AIzaSyBmu2QY7Qz7dh8UQdvUIPZO2e5nc2nlm9g';

type Coordinates = { longitude: number, latitude: number; };

@Component({
  selector: 'app-centre-finder-location',
  templateUrl: './centre-finder-location.component.html',
  styleUrls: ['./centre-finder-location.component.scss']
})
export class CentreFinderLocationComponent implements OnInit {
  @Input() addressType: string;
  @Output() setAddress: EventEmitter<Coordinates> = new EventEmitter();
  @ViewChild('locationText') locationText: any;

  faLocationArrow: IconDefinition = faLocationArrow;
  faSearchLocation: IconDefinition = faSearchLocation;
  showGeolocationLink: boolean = !!navigator.geolocation;
  autocompleteInput: string;
  queryWait: boolean;

  constructor() { }

  ngOnInit(): void {
  }


  ngAfterViewInit(): void {
    this.loadApi();
  }

  private async loadApi() {
    const googleApi = await GoogleMapsApiLoader({
      libraries: ['places'],
      apiKey
    });

    this.getPlaceAutocomplete(googleApi);
  }

  private getPlaceAutocomplete(googleApi) {
    const autocomplete = new googleApi.maps.places.Autocomplete(this.locationText.nativeElement, {
      componentRestrictions: { country: 'UK' },
      types: ['geocode']
    });

    googleApi.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      const latitude: number = place.geometry.location.lat();
      const longitude: number = place.geometry.location.lng();
      const pos: Coordinates = { longitude, latitude };

      this.setAddress.emit(pos);
    });
  }

  getLocation() {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        const pos: Coordinates = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };

        this.setAddress.emit(pos);
      }, function (error) {
        console.error(error);
      });
    } else {
      console.log('not supported');
    }
  }

}
