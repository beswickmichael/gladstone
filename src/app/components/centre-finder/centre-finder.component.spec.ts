import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentreBlockComponent } from './centre-block.component';

describe('CentreBlockComponent', () => {
  let component: CentreBlockComponent;
  let fixture: ComponentFixture<CentreBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentreBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentreBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
