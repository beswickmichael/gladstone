import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {
  faLocationArrow,
  faSearchLocation,
  IconDefinition
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-centre-finder',
  templateUrl: './centre-finder.component.html',
  styleUrls: ['./centre-finder.component.scss']
})
export class CentreFinderComponent implements OnInit {
  @Input() addressType: string;
  @Output() setAddress: EventEmitter<Coordinates> = new EventEmitter();
  @ViewChild('locationText') locationText: any;

  faLocationArrow: IconDefinition = faLocationArrow;
  faSearchLocation: IconDefinition = faSearchLocation;
  showGeolocationLink: boolean = !!navigator.geolocation;
  autocompleteInput: string;
  queryWait: boolean;

  constructor() { }

  ngOnInit() {
  }


  ngAfterViewInit() {
  }

}
