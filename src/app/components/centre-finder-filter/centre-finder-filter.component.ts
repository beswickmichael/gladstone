import { Component, OnInit } from '@angular/core';
import {
  faDraftingCompass,
  faAngleDoubleDown,
  faAngleDoubleUp,
} from '@fortawesome/free-solid-svg-icons';

const tags = [
  'Gym',
  'Swim',
  'Racket Sports',
  'Court',
  'Soft Play',
  'Creche',
  'Climbing Wall',
];

@Component({
  selector: 'app-centre-finder-filter',
  templateUrl: './centre-finder-filter.component.html',
  styleUrls: ['./centre-finder-filter.component.scss']
})
export class CentreFinderFilterComponent implements OnInit {
  faDraftingCompass = faDraftingCompass;
  tags = tags;
  selectedTags: string[];

  constructor() { }

  ngOnInit(): void {
    this.selectedTags = [];
  }

  isTagSelected(tag) {
    return this.selectedTags.includes(tag);
  }

  toggleTag(tag) {
    const isTagSelected = this.isTagSelected(tag);
    
    if (isTagSelected) {
      this.selectedTags = this.selectedTags.filter(x => x !== tag);
    }
    else {
      this.selectedTags.push(tag);
    }
  }
}
