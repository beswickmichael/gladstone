import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberBlockAddComponent } from './member-block-add.component';

describe('MemberBlockAddComponent', () => {
  let component: MemberBlockAddComponent;
  let fixture: ComponentFixture<MemberBlockAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberBlockAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberBlockAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
