import { Component, OnInit } from '@angular/core';
import {
  faPen,
  faLocationArrow,
  faQuestionCircle,
  faUser,
  faCheck,
  faSearchLocation,
  faDraftingCompass
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-member-add',
  templateUrl: './member-add.component.html',
  styleUrls: ['./member-add.component.scss']
})
export class MemberAddComponent implements OnInit {
  faPen = faPen;
  faLocationArrow = faLocationArrow;
  faQuestionCircle = faQuestionCircle;
  faUser = faUser;
  faCheck = faCheck;
  faSearchLocation = faSearchLocation;
  faDraftingCompass= faDraftingCompass;

  constructor() { }

  ngOnInit(): void {
  }

}
