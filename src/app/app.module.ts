import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MembershipComponent } from './routes/membership/membership.component';
import { CentreFinderComponent } from './components/centre-finder/centre-finder.component';
import { CentreFinderFilterComponent } from './components/centre-finder-filter/centre-finder-filter.component';
import { CentreFinderResultsComponent } from './components/centre-finder-results/centre-finder-results.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { HighlightPipe } from './components/autocomplete/highlight.pipe';
import { FormsModule } from '@angular/forms';
import { CentreFinderLocationComponent } from './components/centre-finder-location/centre-finder-location.component';
import { CollapsibleBlockComponent } from './components/collapsible-block/collapsible-block.component';

@NgModule({
  declarations: [
    AppComponent,
    MembershipComponent,
    CentreFinderComponent,
    CentreFinderFilterComponent,
    CentreFinderResultsComponent,
    AutocompleteComponent,
    HighlightPipe,
    CentreFinderLocationComponent,
    CollapsibleBlockComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
