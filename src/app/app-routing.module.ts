import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { MembershipComponent } from './routes/membership/membership.component';


const routes: Routes = [
  {
    path: "join",
    component: MembershipComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
